package com.oppo.finance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class OppoCommonloggingServiceApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(OppoCommonloggingServiceApplication.class, args);
	}

}
