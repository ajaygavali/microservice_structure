# create databases
CREATE DATABASE IF NOT EXISTS `loggingservice`;
CREATE USER 'oppo'@'%' IDENTIFIED BY 'P@ssw0rd';
GRANT ALL ON *.* TO 'oppo'@'%';
FLUSH PRIVILEGES;