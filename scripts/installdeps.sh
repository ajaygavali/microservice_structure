#!/bin/bash
PATH=/usr/local/bin:/usr/local/sbin:~/bin:/usr/bin:/bin:/usr/sbin:/sbin


exists()
{
  command -v "$1" >/dev/null 2>&1
}
echo "Looking for updates"
sudo apt-get update -y
echo "Checking and installing dependencies"

if exists docker; then
    echo 'docker exists!'
else
    echo "docker is not present, installing latest docker CE"
    sudo curl -fsSL https://get.docker.com | sh
fi

if exists nginx; then
  echo 'nginx exists!'
else
    echo "nginx is not present, installing nginx"
    sudo apt-get install -y nginx
fi

if exists java; then
  echo 'Java exists!'
else
    echo "java is not present, installing default-jre"
    sudo apt-get install -y default-jre
fi

if exists mvn; then
    echo 'Maven exists!'
else
    echo "mvn is not present, Installing"
    sudo apt-get install -y maven
fi

if exists docker-compose; then
    echo 'docker-compose exists!'
else
    echo "docker-compose is not present, Installing"
    sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
fi

echo "Looking for images, stopping and removing them"
sudo docker stop $(docker ps -aq) || true &&  docker rm $(docker ps -aq) || true
echo "Starting build"