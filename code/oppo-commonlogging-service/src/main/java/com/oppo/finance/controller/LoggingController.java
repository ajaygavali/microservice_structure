package com.oppo.finance.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oppo.finance.exception.BaseException;
import com.oppo.finance.exception.ExceptionHandler;
import com.oppo.finance.model.request.BackendLoggingRequest;
import com.oppo.finance.response.APIResponse;
import com.oppo.finance.service.LoggingService;

@RestController
@RequestMapping("/logdata")
public class LoggingController {
	
	@Autowired
	LoggingService loggingService;
	
	
	@GetMapping("/healthcheck")
	String healthcheck() {

		return "Logging-Health Checkup working fine";
	}
	
}
